# Proceso de instalación de Eclipse en Windows

Esta guía documenta el proceso de instalación de Eclipse en Windows.

## Requisitos previos

Para poder instalar Eclipse, requerimos del JDK (Java Development Kit). Este contiene todas las herramientas y librerías necesarias para desarrollar en Java, además del entorno de ejecución JRE (Java Runtime Enviroment). **Cualquier versión mayor o igual a la 8 es útil para este curso.**

### Instalación del JDK

1. Descargar el JDK:
  
    - Si tienes Windows 10 de 64 bits, descárgalo de [aquí](https://www.oracle.com/java/technologies/javase-jdk14-downloads.html)
    - Si tienes Windows 10 de 32 bits o inferior, utiliza éste [enlace](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
    - Puedes obtener un listado de otras versiones de Java que puedes descargar [aquí](https://www.oracle.com/java/technologies/javase-downloads.html)

2. Ejecutar el instalador y seguir los pasos.
3. Agregar los binarios del JDK a la variable de entorno `PATH`. Para ello:
   
   1. Dirigirse al "Panel de Control"
   2. Luego a la opción "Sistema".
   3. Hacer click en "Avanzado" y luego "Variables de entorno"
   4. Dentro de "Variables del Sistema", agregar la ruta a la carpeta `bin` que se encuentra dentro del directorio de instalación del JDK a la variables `PATH`
   
   > Una variable de entorno, como su nombre indica, es una variable que forma parte del entorno de ejecución de un proceso dentro del sistema operativo. Las mismas pueden afectar el comportamiento de un proceso. Entre ellas, podemos destacar la variable `PATH` que indica en cuáles directorios el sistema operativo debe buscar archivos ejecutables si no se indica la ruta completa al mismo. Cada uno de los directorios está separado por el caracter `;`. Si el mismo contiene espacios, lo encerramos entre comillas.  
   > Para nuestro caso en particular, el valor de la variable quedaría de forma similar a lo siguiente:
   > ```
   > C:\WINDOWS\system32;C:\WINDOWS;"C:\Program Files\Java\jdk-14\bin"
   > ```       
   > Así, por ejemplo, si en una consola de Windows escribimos lo siguiente:
   > ```bat
   > C:\>javac
   > ```
   > Windows verificará en cada una de las rutas definidas en dicha variable la presencia del ejecutable `javac`.

## Instalación de Eclipse

Para la instalación de Eclipse, debemos tener en cuenta las siguientes cuestiones:

- Puede ser descargado como un archivo comprimido o puede ser instalado mediante un instalador. Nosotros tomaremos la primer alternativa.
- Existen varias distribuciones de Eclipse. Nosotros emplearemos la destinada a desarrolladores Java.
- Las versiones más recientes de Eclipse requieren un sistema operativo de 64 bits.

Para instalarlo, simplemente descarga [este archivo](https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2020-06/R/eclipse-java-2020-06-R-win32-x86_64.zip) y descomprímelo donde desees instalarlo. Si posee una versión de 32 bits de Windows, puedes utilizar este [enlace](https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2018-09/R/eclipse-java-2018-09-win32.zip).